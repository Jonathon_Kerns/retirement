﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Retirement_Application.Contexts;
using Retirement_Application.Models;

namespace Retirement_Application.Controllers
{
    public class EscalsController : Controller
    {
        private readonly CTHCPRDContext _context;

        public EscalsController(CTHCPRDContext context)
        {
            _context = context;
        }

        public ActionResult EscalsForm()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EscalsResults(string emplid)

        {


            var escals = _context.Escals.Where(e => e.Emplid == emplid)
                .OrderBy(e => e.Year)
                .AsNoTracking();


            return View(await escals.ToListAsync());


        }

        // GET: Escals
        public async Task<IActionResult> Index()
        {
            return View(await _context.Escals.ToListAsync());
        }

        // GET: Escals/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var escal = await _context.Escals
                .FirstOrDefaultAsync(m => m.Emplid == id);
            if (escal == null)
            {
                return NotFound();
            }

            return View(escal);
        }

        // GET: Escals/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Escals/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Emplid,Year,Name,SumAmount")] Escal escal)
        {
            if (ModelState.IsValid)
            {
                _context.Add(escal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(escal);
        }

        // GET: Escals/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var escal = await _context.Escals.FindAsync(id);
            if (escal == null)
            {
                return NotFound();
            }
            return View(escal);
        }

        // POST: Escals/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Emplid,Year,Name,SumAmount")] Escal escal)
        {
            if (id != escal.Emplid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(escal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EscalExists(escal.Emplid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(escal);
        }

        // GET: Escals/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var escal = await _context.Escals
                .FirstOrDefaultAsync(m => m.Emplid == id);
            if (escal == null)
            {
                return NotFound();
            }

            return View(escal);
        }

        // POST: Escals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var escal = await _context.Escals.FindAsync(id);
            _context.Escals.Remove(escal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EscalExists(string id)
        {
            return _context.Escals.Any(e => e.Emplid == id);
        }
    }
}
