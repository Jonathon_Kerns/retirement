﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Retirement_Application.Contexts;
using Retirement_Application.Models;

namespace Retirement_Application.Controllers
{
    public class CotAdjReasonsController : Controller
    {
        private readonly CTHCPRDContext _context;

        public CotAdjReasonsController(CTHCPRDContext context)
        {
            _context = context;
        }


        public ActionResult CotAdjReasonsForm()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CotAdjReasonsResults(string emplid, string effectiveDate)

        {


            var cotAdjReasons = _context.CotAdjReasons.Where(e => e.Emplid == emplid)
                .Where(e => Convert.ToInt32(e.Effdt) <= Convert.ToInt32(effectiveDate))
                .OrderBy(e => e.Effdt)
                .AsNoTracking();


            return View(await cotAdjReasons.ToListAsync());


        }


        // GET: CotAdjReasons
        public async Task<IActionResult> Index()
        {
            return View(await _context.CotAdjReasons.ToListAsync());
        }

        // GET: CotAdjReasons/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cotAdjReason = await _context.CotAdjReasons
                .FirstOrDefaultAsync(m => m.Emplid == id);
            if (cotAdjReason == null)
            {
                return NotFound();
            }

            return View(cotAdjReason);
        }

        // GET: CotAdjReasons/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CotAdjReasons/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Emplid,Effdt,CotAdjReason1")] CotAdjReason cotAdjReason)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cotAdjReason);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cotAdjReason);
        }

        // GET: CotAdjReasons/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cotAdjReason = await _context.CotAdjReasons.FindAsync(id);
            if (cotAdjReason == null)
            {
                return NotFound();
            }
            return View(cotAdjReason);
        }

        // POST: CotAdjReasons/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Emplid,Effdt,CotAdjReason1")] CotAdjReason cotAdjReason)
        {
            if (id != cotAdjReason.Emplid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cotAdjReason);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CotAdjReasonExists(cotAdjReason.Emplid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cotAdjReason);
        }

        // GET: CotAdjReasons/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cotAdjReason = await _context.CotAdjReasons
                .FirstOrDefaultAsync(m => m.Emplid == id);
            if (cotAdjReason == null)
            {
                return NotFound();
            }

            return View(cotAdjReason);
        }

        // POST: CotAdjReasons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var cotAdjReason = await _context.CotAdjReasons.FindAsync(id);
            _context.CotAdjReasons.Remove(cotAdjReason);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CotAdjReasonExists(string id)
        {
            return _context.CotAdjReasons.Any(e => e.Emplid == id);
        }
    }
}
