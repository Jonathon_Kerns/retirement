﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Retirement_Application.Migrations
{
    public partial class _20210408 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PS_COT_PENSION_TBL",
                columns: table => new
                {
                    EMPLID = table.Column<string>(type: "varchar(11)", unicode: false, maxLength: 11, nullable: false),
                    EFFDT = table.Column<DateTime>(type: "datetime", nullable: false),
                    EFFSEQ = table.Column<short>(type: "smallint", nullable: false),
                    RUNCNTLID = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: false),
                    LTD_POL_PT = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_POL_AT = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_FIRE_PT = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_FIRE_AT = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_GEN_PT = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_GEN_AT = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_INT = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_PT_REFUND = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_AT_REFUND = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_INT_REFUND = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_PT_ROLL = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    LTD_INT_ROLL = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    FYTD_POL_PT = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    FYTD_POL_AT_BB = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    FYTD_FIRE_PT = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    FYTD_FIRE_AT_BB = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    FYTD_GEN_PT = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    FYTD_GEN_AT_BB = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    POL_SS_PENS = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    FIRE_SS_PENS = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    GEN_SS_PENS = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    PENS_ENTRY_DT = table.Column<DateTime>(type: "datetime", nullable: true),
                    COLA_ELIG_DT = table.Column<DateTime>(type: "datetime", nullable: true),
                    RETIRE_FLAG = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    DEATH_BEN_FLS = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    DEATH_BEN_CALC = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    MONTH_COMPRATE = table.Column<decimal>(type: "decimal(8,2)", nullable: false),
                    PRIOR_SERVICE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    COT_ADJ_REASON = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: false),
                    COT_BUYBACK_CD = table.Column<string>(type: "varchar(6)", unicode: false, maxLength: 6, nullable: false),
                    ELIG_DATE = table.Column<DateTime>(type: "datetime", nullable: true),
                    COT_CITY_PED = table.Column<DateTime>(type: "datetime", nullable: true),
                    COT_OPT_SELECTED = table.Column<string>(type: "varchar(4)", unicode: false, maxLength: 4, nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "PS_EARNINGS_TBL",
                columns: table => new
                {
                    ERNCD = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    EFFDT = table.Column<DateTime>(type: "datetime", nullable: false),
                    EFF_STATUS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    DESCR = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: false),
                    DESCRSHORT = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: false),
                    ERN_SEQUENCE = table.Column<short>(type: "smallint", nullable: false),
                    MAINTAIN_BALANCES = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    BUDGET_EFFECT = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    ALLOW_EMPLTYPE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAYMENT_TYPE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    HRLY_RT_MAXIMUM = table.Column<decimal>(type: "decimal(18,6)", nullable: false),
                    PERUNIT_OVR_RT = table.Column<decimal>(type: "decimal(18,6)", nullable: false),
                    EARN_FLAT_AMT = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    ADD_GROSS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_FWT = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_FICA = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_FUT = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_CIT = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_CUI = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_CUI_HOURS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_CPP = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_QIT = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_QPP = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_TRUE_T4GRS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_TRUE_RVGRS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_PAY_TAX = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_REG = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    WITHHOLD_FWT = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    HRS_ONLY = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SHIFT_DIFF_ELIG = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    TAX_GRS_COMPNT = table.Column<string>(type: "varchar(5)", unicode: false, maxLength: 5, nullable: false),
                    SPEC_CALC_RTN = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    FACTOR_MULT = table.Column<decimal>(type: "decimal(9,4)", nullable: false),
                    FACTOR_RATE_ADJ = table.Column<decimal>(type: "decimal(9,4)", nullable: false),
                    FACTOR_HRS_ADJ = table.Column<decimal>(type: "decimal(9,4)", nullable: false),
                    FACTOR_ERN_ADJ = table.Column<decimal>(type: "decimal(9,4)", nullable: false),
                    GL_EXPENSE = table.Column<string>(type: "varchar(35)", unicode: false, maxLength: 35, nullable: false),
                    SUBTRACT_EARNS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    DEDCD_PAYBACK = table.Column<string>(type: "varchar(6)", unicode: false, maxLength: 6, nullable: false),
                    TAX_METHOD = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    EARN_YTD_MAX = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    BASED_ON_TYPE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    BASED_ON_ERNCD = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    BASED_ON_ACC_ERNCD = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    AMT_OR_HOURS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    ELIG_FOR_RETROPAY = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    USED_TO_PAY_RETRO = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    EFFECT_ON_FLSA = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    FLSA_CATEGORY = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    REG_PAY_INCLUDED = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    TIPS_CATEGORY = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    ADD_DE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_T4A = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_RV2 = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_BENEFITS_RATE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_CPDF_ERNCD = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_OTH_PAY = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_PAY_CAP = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_PREM_PAY = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_OT_PAY_IND = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_ADD_TO_SF50_52 = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_INCLUDE_LOC = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_IRR_REPORTABLE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_IRR_LWOP = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_SF113A_LUMPSUM = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_SF113A_WAGES = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_FEFFLA = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_FMLA = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_LV_EARN_TYPE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    INCOME_CD_1042 = table.Column<string>(type: "varchar(2)", unicode: false, maxLength: 2, nullable: false),
                    PERMANENCY_NLD = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    TAX_CLASS_NLD = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    HRS_DIST_SW = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    HP_ADMINSTIP_FLAG = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SUBJECT_QPIP = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PNA_USE_SGL_EMPL = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "PS_PAY_CHECK",
                columns: table => new
                {
                    COMPANY = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    PAYGROUP = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    PAY_END_DT = table.Column<DateTime>(type: "datetime", nullable: false),
                    OFF_CYCLE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAGE_NUM = table.Column<int>(type: "int", nullable: false),
                    LINE_NUM = table.Column<short>(type: "smallint", nullable: false),
                    SEPCHK = table.Column<short>(type: "smallint", nullable: false),
                    FORM_ID = table.Column<string>(type: "varchar(6)", unicode: false, maxLength: 6, nullable: false),
                    PAYCHECK_NBR = table.Column<decimal>(type: "decimal(15,0)", nullable: false),
                    EMPLID = table.Column<string>(type: "varchar(11)", unicode: false, maxLength: 11, nullable: false),
                    EMPL_RCD = table.Column<short>(type: "smallint", nullable: false),
                    NAME = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    DEPTID = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: false),
                    EMPL_TYPE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SSN = table.Column<string>(type: "varchar(9)", unicode: false, maxLength: 9, nullable: false),
                    SIN = table.Column<string>(type: "varchar(9)", unicode: false, maxLength: 9, nullable: false),
                    TOTAL_GROSS = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    TOTAL_TAXES = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    TOTAL_DEDUCTIONS = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    NET_PAY = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    CHECK_DT = table.Column<DateTime>(type: "datetime", nullable: true),
                    PAYCHECK_STATUS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAYCHECK_OPTION = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAYCHECK_ADJUST = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAYCHECK_REPRINT = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAYCHECK_CASHED = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAYCHECK_ADDR_OPTN = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAYCHECK_NAME = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    COUNTRY = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    ADDRESS1 = table.Column<string>(type: "varchar(55)", unicode: false, maxLength: 55, nullable: false),
                    ADDRESS2 = table.Column<string>(type: "varchar(55)", unicode: false, maxLength: 55, nullable: false),
                    ADDRESS3 = table.Column<string>(type: "varchar(55)", unicode: false, maxLength: 55, nullable: false),
                    ADDRESS4 = table.Column<string>(type: "varchar(55)", unicode: false, maxLength: 55, nullable: false),
                    CITY = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: false),
                    NUM1 = table.Column<string>(type: "varchar(6)", unicode: false, maxLength: 6, nullable: false),
                    NUM2 = table.Column<string>(type: "varchar(6)", unicode: false, maxLength: 6, nullable: false),
                    HOUSE_TYPE = table.Column<string>(type: "varchar(2)", unicode: false, maxLength: 2, nullable: false),
                    ADDR_FIELD1 = table.Column<string>(type: "varchar(2)", unicode: false, maxLength: 2, nullable: false),
                    ADDR_FIELD2 = table.Column<string>(type: "varchar(4)", unicode: false, maxLength: 4, nullable: false),
                    ADDR_FIELD3 = table.Column<string>(type: "varchar(4)", unicode: false, maxLength: 4, nullable: false),
                    COUNTY = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: false),
                    STATE = table.Column<string>(type: "varchar(6)", unicode: false, maxLength: 6, nullable: false),
                    POSTAL = table.Column<string>(type: "varchar(12)", unicode: false, maxLength: 12, nullable: false),
                    GEO_CODE = table.Column<string>(type: "varchar(11)", unicode: false, maxLength: 11, nullable: false),
                    IN_CITY_LIMIT = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    LOCATION = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: false),
                    PAYCHECK_DIST_KEY1 = table.Column<string>(type: "varchar(90)", unicode: false, maxLength: 90, nullable: false),
                    PAYCHECK_DIST_KEY2 = table.Column<string>(type: "varchar(90)", unicode: false, maxLength: 90, nullable: false),
                    BENEFIT_RCD_NBR = table.Column<short>(type: "smallint", nullable: false),
                    PAY_SHEET_SRC = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    BUSINESS_UNIT = table.Column<string>(type: "varchar(5)", unicode: false, maxLength: 5, nullable: false),
                    GVT_SCHEDULE_NO = table.Column<string>(type: "varchar(14)", unicode: false, maxLength: 14, nullable: false),
                    GVT_PAY_ID_LINE_1 = table.Column<string>(type: "varchar(40)", unicode: false, maxLength: 40, nullable: false),
                    GVT_PAY_ID_LINE_2 = table.Column<string>(type: "varchar(40)", unicode: false, maxLength: 40, nullable: false),
                    GVT_ECS_LTD_INDIC = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    GVT_ECS_OFF_ACCT = table.Column<string>(type: "varchar(16)", unicode: false, maxLength: 16, nullable: false),
                    GVT_RITS_DT = table.Column<DateTime>(type: "datetime", nullable: true),
                    GVT_TSP_SEQ_YR = table.Column<short>(type: "smallint", nullable: false),
                    GVT_TSP_SEQ_NO = table.Column<short>(type: "smallint", nullable: false),
                    PROVINCE = table.Column<string>(type: "varchar(6)", unicode: false, maxLength: 6, nullable: false),
                    HP_CORRECT_STATUS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    HP_CORRECTED_DT = table.Column<DateTime>(type: "datetime", nullable: true),
                    HR_WP_PROCESS_FLG = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    UPDATE_DT = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "PS_PAY_OTH_EARNS",
                columns: table => new
                {
                    COMPANY = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    PAYGROUP = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    PAY_END_DT = table.Column<DateTime>(type: "datetime", nullable: false),
                    OFF_CYCLE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    PAGE_NUM = table.Column<int>(type: "int", nullable: false),
                    LINE_NUM = table.Column<short>(type: "smallint", nullable: false),
                    ADDL_NBR = table.Column<short>(type: "smallint", nullable: false),
                    ERNCD = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    RATE_USED = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    SEPCHK = table.Column<short>(type: "smallint", nullable: false),
                    JOB_PAY = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    OTH_HRS = table.Column<decimal>(type: "decimal(6,2)", nullable: false),
                    OTH_PAY = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    OTH_EARNS = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    ADD_GROSS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    TAX_METHOD = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    ADDL_SEQ = table.Column<short>(type: "smallint", nullable: false),
                    TL_SOURCE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    BAS_CREDIT_SW = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    COMP_RATECD = table.Column<string>(type: "varchar(6)", unicode: false, maxLength: 6, nullable: false),
                    COMPRATE = table.Column<decimal>(type: "decimal(18,6)", nullable: false),
                    COMPRATE_USED = table.Column<decimal>(type: "decimal(18,6)", nullable: false),
                    HRS_DIST_SW = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    XREF_NUM = table.Column<decimal>(type: "decimal(15,0)", nullable: false),
                    EX_DOC_ID = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: false),
                    EX_DOC_TYPE = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    EX_LINE_NBR = table.Column<int>(type: "int", nullable: false),
                    CURRENCY_CD = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
                    VC_PLAN_ID = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: false),
                    VC_PAYOUT_PRD_ID = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: false),
                    GB_GROUP_ID = table.Column<string>(type: "varchar(15)", unicode: false, maxLength: 15, nullable: false),
                    APPLID = table.Column<string>(type: "varchar(11)", unicode: false, maxLength: 11, nullable: false),
                    AWARD_DATE = table.Column<DateTime>(type: "datetime", nullable: true),
                    NOTIFY_STATUS = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
                    EIM_KEY = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
                    PU_SOURCE = table.Column<string>(type: "varchar(2)", unicode: false, maxLength: 2, nullable: false),
                    RETROPAY_SEQ_NO = table.Column<string>(type: "varchar(15)", unicode: false, maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "SalaryHistory",
                columns: table => new
                {
                    EMPLID = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    SSN = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Name = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: true),
                    Year = table.Column<int>(type: "int", nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(19,4)", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PS_COT_PENSION_TBL");

            migrationBuilder.DropTable(
                name: "PS_EARNINGS_TBL");

            migrationBuilder.DropTable(
                name: "PS_PAY_CHECK");

            migrationBuilder.DropTable(
                name: "PS_PAY_OTH_EARNS");

            migrationBuilder.DropTable(
                name: "SalaryHistory");
        }
    }
}
