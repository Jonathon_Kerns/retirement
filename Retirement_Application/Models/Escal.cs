﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Retirement_Application.Models
{
    //[Keyless]
    public partial class Escal
    {
		[Key]
        [Column("EMPLID")]
        [StringLength(50)]
        public string Emplid { get; set; }
        [Column("YEAR")]
        public int? Year { get; set; }
        [Column("NAME")]
        [StringLength(25)]
        public string Name { get; set; }
        [Column("SUM_AMOUNT", TypeName = "decimal(38, 4)")]
        public decimal? SumAmount { get; set; }
    }
}
