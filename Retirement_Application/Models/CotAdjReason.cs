﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Retirement_Application.Models
{
    //[Keyless]
    public partial class CotAdjReason
    {
        [Key]
        [Required]
        [Column("EMPLID")]
        [StringLength(11)]
        public string Emplid { get; set; }
        [Column("EFFDT", TypeName = "datetime")]
        public DateTime Effdt { get; set; }
        [Required]
        [Column("COT_ADJ_REASON")]
        [StringLength(100)]
        public string CotAdjReason1 { get; set; }
    }
}
